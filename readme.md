# Origin Interactive


## Getting Started

* Install [Node.js](http://nodejs.org/)
* Install [Gulp](http://gulpjs.com/) command line interface `npm install -g gulp`
* Update `package.json` with your project's details
* Install project dependencies by:
  * `cd root/of/this/project/`
  * `npm install`
  * `npm install requirejs`


## During development
run: `gulp build`

Changes should be made inside `/src/`, any updates you make will be compiled/copied into `/build/`.
 You should view your work by opening the port in your browser.
 Example Server started http://localhost:9001