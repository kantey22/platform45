'use strict';

var gulp = require('gulp'),
	concat = require('gulp-concat'),
	uglify = require('gulp-uglify'),
    jshint = require('gulp-jshint'),
    connect = require('gulp-connect'),
	sourcemaps = require('gulp-sourcemaps'),
    gulpif = require('gulp-if'),
    outputDir = 'build';

gulp.task('scripts', function () {
    return gulp.src('src/js/**/*.js')
    	.pipe(sourcemaps.init())
        .pipe(jshint())
        .pipe(jshint.reporter('jshint-stylish'))
        .pipe(concat('scripts.js'))
        .pipe(uglify())
        .pipe(sourcemaps.write({addComment: true})) //change true to false when builing for production
        .pipe(gulp.dest(outputDir + '/js'))
        .pipe(connect.reload())
});