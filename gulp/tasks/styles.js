'use strict';

var gulp = require('gulp'),
	sass = require('gulp-sass'),
	autoprefixer = require('gulp-autoprefixer'),
	minifyCss = require('gulp-minify-css'),
	connect = require('gulp-connect'),
	sourcemaps = require('gulp-sourcemaps'),
	gulpif = require('gulp-if'),
	outputDir = 'build';

gulp.task('styles', function() {
  	return gulp.src('src/scss/**/*.scss')
  			.pipe(sourcemaps.init())
			.pipe(sass({
				outputStyle: 'compressed',
				includePaths : ['node_modules/susy/sass']
			}).on('error', sass.logError))
			.pipe(autoprefixer({
	            browsers: ['last 2 versions'],
	            cascade: false
	        }))
			.pipe(sourcemaps.write({addComment: true})) //change true to false when builing for production
		    .pipe(gulp.dest(outputDir + '/css'))
			.pipe(connect.reload())
			
});