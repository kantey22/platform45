'use strict';

var gulp = require('gulp'),
  	connect = require('gulp-connect');

gulp.task('watch', ['connect', 'styles'], function () {
  gulp.watch('src/scss/**/*.scss', ['styles']);
  gulp.watch('src/**/*.html', ['html']);
  gulp.watch('src/js/**/*.js', ['scripts'], connect.reload);
  gulp.watch('src/fonts/**/*', ['fonts'], connect.reload);
  gulp.watch('src/images/**/*.+(png|jpg|jpeg|gif|svg)', ['images'], connect.reload);
  gulp.watch("src/*.html").on('change', connect.reload);
});