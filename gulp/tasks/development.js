'use strict';

var gulp = require('gulp'),
	runSequence = require('run-sequence');

gulp.task('build', function(cb) {
  runSequence('clean:build', ['styles', 'scripts', 'useref', 'images', 'fonts', 'html', 'connect','watch'], cb)
});