'use strict';

var gulp = require('gulp'),
	del = require('del'),
	runSequence = require('run-sequence');

gulp.task('clean:build', function() {
  return del.sync('build');
});