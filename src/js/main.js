// Icon Toggle
var iconClick = document.getElementById('usericon');
iconClick.onclick =  function (e){
	var wrapper = document.querySelector('.user-form');

	if (wrapper.classList.contains('open'))
	{
		wrapper.classList.remove('open');
		iconClick.removeAttribute("class", "icon-text-close");
		iconClick.setAttribute("class","user-info-icon");
		

	} else {
		wrapper.classList.add('open');
		iconClick.setAttribute("class", "icon-text-close");
		

	}
}

// Form Validations

var validations = {
  required: function(value){
    return value !== '';
  },
  phone: function(value){
    return value.match(/^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im
);
  },
  email: function(value){
    return value.match(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
  }
}

function validate() {
  var form = document.getElementById('form'),
      inputsArr = form.querySelectorAll('input'),
      errorMessage = document.querySelector(".error"),
      successMessage = document.querySelector(".success");
  
  form.addEventListener('submit', function(e){
    var i = 0;
    while (i < inputsArr.length) {
      var attr = inputsArr[i].getAttribute('data-validation'),
          rules = attr ? attr.split(' ') : '',
					parent = inputsArr[i].closest("input"),
					j = 0;
			
      while (j < rules.length) {
        if(!validations[rules[j]](inputsArr[i].value)) {
          e.preventDefault();
          
          errorMessage.className = "ui error message";
          errorMessage.innerHTML = "Invalid '" + inputsArr[i].name + "'";
          parent.className = "input error";
          return false;
        }
        errorMessage.className = "error message hidden";
        parent.className = "input";
        j++;
      }
      i++;
    }
    e.preventDefault();
    successMessage.className = "ui success message";
    form.outerHTML = "";
    delete form;
  }, false)
}
validate();
